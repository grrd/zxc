package main

// Script is a script
type Script struct {
	Name string `toml:"name"`
	Exec string `toml:"exec"`
	Desc string `toml:"desc"`
}

// NewScript creates a new script
func NewScript(name, exec, desc string) *Script {
	return &Script{name, exec, desc}
}

func (s *Script) Execute() error {
	return nil
}
