package cmd

import (
	libcmd "github.com/spf13/cobra"
)

var runcmd = &libcmd.Command{
	Use:     "run",
	Short:   "run a script specified in the scripts manifest file",
	PreRunE: prerunRun,
	RunE:    runRun,
}

func prerunRun(cmd *libcmd.Command, args []string) error {
	return nil
}

func runRun(cmd *libcmd.Command, args []string) error {
	return nil
}
