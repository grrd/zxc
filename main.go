package main

import (
	"fmt"
	"log"
	"os/exec"

	_ "gitlab.com/grrd/zxc/cmd"
)

func main() {
	cmd := exec.Command("echo", "wew")
	out, err := cmd.Output()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(out))
}
