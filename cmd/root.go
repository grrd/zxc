package cmd

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
	libcmd "github.com/spf13/cobra"
	_ "github.com/spf13/viper"
)

var (
	// flags
	optVerbose bool
)

var (
	validFilenames = map[string]bool{
		"zxc.toml":      true,
		"commands.toml": true,
		"scripts.toml":  true,
		"project.toml":  true,
	}
)

func init() {
	rootcmd.AddCommand(runcmd)
	rootcmd.PersistentFlags().BoolVarP(&optVerbose, "verbose", "v", false, "verbose output")
}

var rootcmd = &libcmd.Command{
	Use:     "zxc",
	Short:   "zxc is a repository script manager",
	PreRunE: prerunRoot,
}

// Execute runs the root command 'zxc'
func Execute() {
	if err := rootcmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func prerunRoot(cmd *libcmd.Command, args []string) error {
	cwd, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		return err
	}

	scriptsManifest, err := getScriptsManifest(cwd)
	if err != nil {
		return err
	}

	fmt.Println(scriptsManifest)
	return nil
}

func getScriptsManifest(cwd string) (string, error) {
	if optVerbose {
		log.Printf("Searching for scripts manifest in %s\n", cwd)
	}

	fs, err := ioutil.ReadDir(cwd)
	if err != nil {
		return "", errors.Wrap(err, "unable to read current working directory")
	}

	// match all files in the current directory against the valid filenames
	for _, f := range fs {
		if _, exists := validFilenames[f.Name()]; exists && !f.IsDir() {
			// found the manifest
			fpath, _ := filepath.Abs(fmt.Sprintf("%s/%s", cwd, f.Name()))
			return fpath, nil
		}
	}

	if cwd != "/" {
		// navigate to the parent directory and continue searching
		parentdir, _ := filepath.Abs(fmt.Sprintf("%s/..", cwd))
		return getScriptsManifest(parentdir)
	}

	// end of line has been reached and no manifest was found
	return "", errors.New("unable to find scripts manifest")
}
