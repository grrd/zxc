package main

import (
	"github.com/BurntSushi/toml"
	"github.com/pkg/errors"
)

type (
	ManifestFile struct {
		Scripts []Script `toml:"scripts"`
	}

	// Manifest is the scripts manifest
	Manifest struct {
		Variables map[string]string
		Scripts   map[string]Script `toml:"scripts"`
	}
)

// NewManifest creates a new manifest
func NewManifest(path string) (*Manifest, error) {
	var manifestFile ManifestFile
	if _, err := toml.DecodeFile(path, &manifestFile); err != nil {
		return nil, errors.Wrap(err, "unable to parse manifest file")
	}

	scripts := map[string]Script{}
	for _, s := range manifestFile.Scripts {
		scripts[s.Name] = s
	}

	return &Manifest{}, nil
}
